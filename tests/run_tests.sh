#!/bin/bash

TEST_DATA=test_data
TEST_OUTPUT=test_output
rm -rf "${TEST_OUTPUT}"
mkdir -p "${TEST_OUTPUT}"

EXIT_STATUS=0

function check_test_results() {
    if [ $EXIT_CODE -ne $EXPECTED_EXIT_CODE ];then
        EXIT_STATUS=1
        echo "${TEST} EXIT($EXIT_CODE) != EXPECTED($EXPECTED_EXIT_CODE) FAILED!"
    else
        echo "${TEST} EXIT($EXIT_CODE) PASSED"
    fi
    for OUTPUT in $OUTPUTS; do
        filename=$(basename "$OUTPUT")
        extension="${filename##*.}"
        if [[ $extension == 'gz' ]]; then
            diff <(gzip -dc "$TEST_OUTPUT/${TEST}_${OUTPUT}") <(gzip -dc "${TEST_DATA}/${TEST}_${OUTPUT}")
        else
            diff "${TEST_OUTPUT}/${TEST}_${OUTPUT}" "${TEST_DATA}/${TEST}_${OUTPUT}"
        fi
        if [ $? -ne 0 ]; then
            EXIT_STATUS=1
            echo "${TEST}_${OUTPUT} FAILED!"
        else
            echo "${TEST}_${OUTPUT} PASSED"
        fi
    done
    for NOT_OUTPUT in $NOT_OUTPUTS; do
        if [ -f "$NOT_OUTPUT" ]
        then
            EXIT_STATUS=1
            echo "${TEST}_${NOT_OUTPUT} FAILED!"
        else
            echo "${TEST}_${NOT_OUTPUT} PASSED"
        fi
    done
}

NOT_OUTPUTS=""

# Test split file output
TEST=test_1
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test separate index file
TEST=test_2
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out BC1-read-2.out BC2-read-2.out BC3-read-2.out BC4-read-2.out unmatched-read-2.out multimatched-read-2.out summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test special characters in sample names (utf-8)
TEST=test_3
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC_1-read-1.out BC_2-read-1.out BC_3-read-1.out BC_4-read-1.out unmatched-read-1.out multimatched-read-1.out BC_1-read-2.out BC_2-read-2.out BC_3-read-2.out BC_4-read-2.out unmatched-read-2.out multimatched-read-2.out summary.out error.out"
NOT_OUTPUTS=""
PYTHONIOENCODING='UTF-8' ../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes_odd_sample_names.txt" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test variable length barcodes
TEST=test_4
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes_vary_length.txt" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=1
check_test_results


# Test gzip input and output
TEST=test_5
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out.gz BC2-read-1.out.gz BC3-read-1.out.gz BC4-read-1.out.gz unmatched-read-1.out.gz multimatched-read-1.out.gz summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq.gz" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --gzipout --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test dual barcodes
TEST=test_6
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.fastq BC1-read-2.fastq BC1-read-3.fastq BC2-read-1.fastq BC2-read-2.fastq BC2-read-3.fastq BC3-read-1.fastq BC3-read-2.fastq BC3-read-3.fastq BC4-read-1.fastq BC4-read-2.fastq BC4-read-3.fastq error.out multimatched-read-1.fastq multimatched-read-2.fastq multimatched-read-3.fastq summary.out unmatched-read-1.fastq unmatched-read-2.fastq unmatched-read-3.fastq"
NOT_OUTPUTS=""
../barcode_splitter.py --bcfile "${TEST_DATA}/barcode_splitter_barcodes_dual.txt" --prefix "${TEST_OUTPUT}/${TEST}_" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" "${TEST_DATA}/barcode_splitter_index_2.fastq" --idxread 2 3 --mismatches 1 --split_all 1> "${TEST_OUTPUT}/${TEST}_summary.out" 2> "${TEST_OUTPUT}/${TEST}_error.out"
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test old Illumina style fastq ids
TEST=test_7
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out BC1-read-2.out BC2-read-2.out BC3-read-2.out BC4-read-2.out unmatched-read-2.out multimatched-read-2.out summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1_oldstyle_ids.fastq" "${TEST_DATA}/barcode_splitter_index_oldstyle_ids.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test new Illumina style fastq ids
TEST=test_8
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out BC1-read-2.out BC2-read-2.out BC3-read-2.out BC4-read-2.out unmatched-read-2.out multimatched-read-2.out summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1_newstyle_ids.fastq" "${TEST_DATA}/barcode_splitter_index_newstyle_ids.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test no splitting of index file by default
TEST=test_9
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out summary.out error.out"
NOT_OUTPUTS="BC1-read-2.out BC2-read-2.out BC3-read-2.out BC4-read-2.out unmatched-read-2.out multimatched-read-2.out"
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test split all files by default when all are index files
TEST=test_10
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test that sequences shorter than barcodes are treated as unmatched
TEST=test_11
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 0 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1_short.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test output sequence file does not exist
TEST=test_12
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "baddir/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=6
check_test_results
#One more check that turns out to be an error on a random outfile
if [ `grep -c 'ERROR: Unable to open baddir' ${TEST_OUTPUT}/${TEST}_error.out` -ne 1 ];then
    EXIT_STATUS=1
    echo "${TEST} FAILED!"
    cat "${TEST_OUTPUT}/${TEST}_error.out"
fi


# Test input sequence file does not exist
TEST=test_13
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "baddir/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=10
check_test_results


# Test input barcode file does not exist
TEST=test_14
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "baddir/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=5
check_test_results


# Test output unmatched file exists
TEST=test_15
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
touch ${TEST_OUTPUT}/${TEST}_unmatched-read-1.out
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=7
check_test_results


# Test output multimatched file exists
TEST=test_16
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
touch ${TEST_OUTPUT}/${TEST}_multimatched-read-1.out
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=8
check_test_results


# Test gzipped output sequence file does not exist
TEST=test_17
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "baddir/${TEST}_" --suffix .out --idxread 1 --split_all --gzipout 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=6
check_test_results
#One more check that turns out to be an error on a random outfile
if [ `grep -c 'ERROR: Unable to open baddir' ${TEST_OUTPUT}/${TEST}_error.out` -ne 1 ];then
    EXIT_STATUS=1
    echo "${TEST} FAILED!"
    cat "${TEST_OUTPUT}/${TEST}_error.out"
fi


# Test gzipped input sequence file does not exist
TEST=test_18
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
# NOTE: Skipping check of error.out since gzip messages can differ bewteen systems
OUTPUTS="summary.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "baddir/barcode_splitter1.fastq.gz" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=1
check_test_results


# Test gzipped output unmatched file exists
TEST=test_19
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
touch ${TEST_OUTPUT}/${TEST}_unmatched-read-1.out.gz
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all --gzipout 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=7
check_test_results


# Test gzipped output multimatched file exists
TEST=test_20
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
touch ${TEST_OUTPUT}/${TEST}_multimatched-read-1.out.gz
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/barcode_splitter1.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all --gzipout 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=8
check_test_results


# Test error for special characters in sample names (utf-8)
TEST=test_21
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes_odd_sample_names.txt" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 2 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=11
#Some versions of Python handle the UTF-8 encoding by default - that's OK,
#others error, in which case we expect to see a pretty error & truncated output
if [ $EXIT_CODE -ne 0 ];then
    check_test_results
else
    echo "${TEST} EXIT($EXIT_CODE) PASSED"
fi


# Test that version in barcode splitter and setup are the same (and that thus,
# single source versioning is working)
TEST=test_22
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS=""
NOT_OUTPUTS=""
../barcode_splitter.py --version &> ${TEST_OUTPUT}/${TEST}_tmp.out
EXIT_CODE=$?
if [ $? -eq 0 ]; then
    cut -f 3 -d ' ' ${TEST_OUTPUT}/${TEST}_tmp.out 1> ${TEST_OUTPUT}/${TEST}_barcodesplitter.out
    rm -f ${TEST_OUTPUT}/${TEST}_tmp.out 2> /dev/null
    ../setup.py --version &> ${TEST_OUTPUT}/${TEST}_setup.out
    EXIT_CODE=$?
fi
EXPECTED_EXIT_CODE=0
#One check to make sure setup and barcode_splitter agree on the version
diff "${TEST_OUTPUT}/${TEST}_barcodesplitter.out" "${TEST_OUTPUT}/${TEST}_setup.out"
if [ $? -ne 0 ]; then
    EXIT_STATUS=1
    echo "${TEST} VERSION(${TEST}_barcodesplitter.out == ${TEST}_setup.out) FAILED!"
else
    echo "${TEST} VERSION(${TEST}_barcodesplitter.out == ${TEST}_setup.out) PASSED"
fi


# Test split file output
TEST=test_23
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out summary.out error.out"
NOT_OUTPUTS=""
mkfifo "${TEST_DATA}/${TEST}_read1_fifo.fastq"
cat "${TEST_DATA}/barcode_splitter1.fastq" > "${TEST_DATA}/${TEST}_read1_fifo.fastq" &
../barcode_splitter.py --mismatches 2 --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" "${TEST_DATA}/${TEST}_read1_fifo.fastq" --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
rm "${TEST_DATA}/${TEST}_read1_fifo.fastq"
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results


# Test duplicate barcode rows
TEST=test_24
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="summary.out error.out"
NOT_OUTPUTS="BC1-read-1.out BC2-read-1.out BC3-read-1.out BC4-read-1.out unmatched-read-1.out multimatched-read-1.out"
../barcode_splitter.py --bcfile "${TEST_DATA}/barcode_splitter_barcodes_dupes.txt" --prefix "${TEST_OUTPUT}/${TEST}_" "${TEST_DATA}/barcode_splitter1.fastq" "${TEST_DATA}/barcode_splitter_index.fastq" "${TEST_DATA}/barcode_splitter_index_2.fastq" --idxread 2 3 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=12
check_test_results


# Test gzip input and output bash process substitution
TEST=test_25
rm -f ${TEST_OUTPUT}/${TEST}_* 2> /dev/null
OUTPUTS="BC1-read-1.out.gz BC2-read-1.out.gz BC3-read-1.out.gz BC4-read-1.out.gz unmatched-read-1.out.gz multimatched-read-1.out.gz summary.out error.out"
NOT_OUTPUTS=""
../barcode_splitter.py --mismatches 2 --gzipin --gzipout --bcfile "${TEST_DATA}/barcode_splitter_barcodes.txt" <(cat "${TEST_DATA}/barcode_splitter1.fastq.gz") --prefix "${TEST_OUTPUT}/${TEST}_" --suffix .out --gzipout --idxread 1 --split_all 2> ${TEST_OUTPUT}/${TEST}_error.out 1> ${TEST_OUTPUT}/${TEST}_summary.out
EXIT_CODE=$?
EXPECTED_EXIT_CODE=0
check_test_results

exit $EXIT_STATUS
