# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.18.6] - 2019-07-22
### Fixed
-   Handle input from file descriptors (e.g. from bash process substitution) @lance_parsons
-   Fall back to internal gzip implementation when external call to gzip fails @lance_parsons

## [0.18.5] - 2019-07-17
### Added
-   Support for Python 3.7 @hepcat72.

## [0.18.4] - 2019-02-15
### Fixed
-   Exit status now zero for usage and version output @lance_parsons.

## [0.18.3] - 2019-02-13
### Added
-   Added detection & reporting of duplicate barcode combinations @hepcat72.
-   Usage updated to explain allowed barcode lengths @hepcat72.

## [0.18.2] - 2017-03-28
### Fixed
-   Limit buffer size for forked gzip processes to avoid huge memory use @lance_parsons.

## [0.18.1] - 2017-03-14
### Fixed
-   No longer exit when input files are symlinks or named pipes @lance_parsons.

## [0.18.0] - 2017-03-02
### Added
-   Python 3 support from @lance_parsons.
-   UTF-8 support for sample names from @lance_parsons.

### Changed
-   Output summary samples in same order as barcodes file from @lance_parsons.
-   Only output the split files for index reads if requested from @hepcat72.
-   Sanitize the sample names for inclusion in a file name from @hepcat72 & @lance_parsons.
-   Transferred to Princeton Genomics Group BitBucket account from @hepcat72.
-   Do not sanitize the specified prefix from @lance_parsons.
-   Output the original, not sanitized sample names in the summary from @lance_parsons.
-   Output filename glob as last column of summary from @lance_parsons.
-   Native (python) gzip now faster from @lance_parsons.
-   No longer process reads when fastq ids do not match from @lance_parsons.
-   Report the number of errors when fastq ids do not match in the summary from @lance_parsons.
-   Report number of errors when fastq files do not have the same number of reads in the summary from @lance_parsons.

### Fixed
-   Handled exception when UTF8 environment variable not set from @hepcat72.
-   Handled file not found exceptions from @hepcat72.
-   Handled case where barcode is longer than reads from @hepcat72.
-   Handled TypeError when printing unmatched_counts from @hepcat72.
-   Throw and error if the barcodes for a given index vary in length (this didn't work reliably before) from @lance_parsons.
-   Fix bug where barcode length was determined incorrectly, resulting in all reads not matching from @lance_parsons.
-   Do not crash when fastq files have varying number of reads from @lance_parsons.

## [0.17] - 2016-11-22
### Added
-   Sanitize sample_id and prefix to remove non-standard characters from @hepcat72.

[Unreleased]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/HEAD%0D0.18.6
[0.18.6]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.6%0D0.18.5
[0.18.5]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.5%0D0.18.4
[0.18.4]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.4%0D0.18.3
[0.18.3]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.3%0D0.18.2
[0.18.2]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.2%0D0.18.1
[0.18.1]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.1%0D0.18.0
[0.18.0]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.18.0%0D0.17
[0.17]: https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/0.17%0D8b2e8cf
