Guidelines for developers
=========================

1.  Code must follow [PEP-8 standards](https://www.python.org/dev/peps/pep-0008/)
2.  All pipelines tests must pass before pull requests will be merged
3.  New functionality must have a corresponding test(s) created
4.  Keep the `CHANGELOG.md` up to date
5.  This project adheres to [Semantic Versioning](http://semver.org/).


Steps to contribute
-------------------

1.  [Fork the repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) or *Synchronize your fork* (see below).

2.  [Enable Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) on your fork

3.  [Clone your fork](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) on your development machine

4.  [Configure an upstream remote](https://help.github.com/articles/configuring-a-remote-for-a-fork/)

5.  [Checkout a new branch](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html)

6.  [Make your changes](https://confluence.atlassian.com/bitbucket/work-on-local-source-files-223217905.html) (see *Releases* below for changes necessary for a new release)

7.  Test your changes locally (see *Running tests* below)

8.  [Push your branch back up to your repository](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html) (`git push origin <feature_branch>`)

9.  Ensure pipelines build successfully and your branch can be merged without conflict.

10. [Create a pull request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html)

11. After PR merge, proceed to *Releases* (see below).

### Synchronize your fork

1.  Be sure you have [configured an upstream remote](https://help.github.com/articles/configuring-a-remote-for-a-fork/)

2.  [Fetch and merge upstream changes](https://help.github.com/articles/syncing-a-fork/) (e.g. `git checkout master; git fetch upstream master; git merge upstream master;`)


Running tests
-------------

You can run the basic functional tests on your development machine by changing to the `tests` directory and running `./run_tests.sh`

We also use [Tox](https://tox.readthedocs.io/en/latest/) to test against multiple python versions. You can run tox tests locally if you have installed tox on your machine.

If you want to better simulate the pipelines tests (and get many python versions installed quickly), you can run the tests using Tox in a docker container.

1.  [Install Docker](https://www.docker.com/products/overview) on your machine

2.  Start up the container:
    ```
    docker run -it --volume="`pwd`:/barcode_splitter" --workdir="/barcode_splitter" --memory=4g --entrypoint=/bin/bash n42org/tox
    ```

3.  You can now run commands listed in the `bitbucket_pipeline.yml` file (e.g. `tox` to run tests on multiple python versions)


Releases
--------

1. *Synchronize your fork* (see above).

2.  Update `__version__` in `barcode_splitter.py`.

3.  Update `CHANGELOG.md` from `[Unreleased]` to `[<version>] - year-mo-dy`

4.  Update diff links in `CHANGELOG.md`.

5.  Push your changes directly (or submit a PR if there are changes other than described above), e.g. `git push upstream master`.

6. Ensure pipelines pass, and then *Synchronize your fork* (see above).

7.  Tag with new version:
    ```
    git tag -fa <version>;
    git push origin --tags;
    git push upstream --tags;
    ```

8.  Create universal wheel:
    ```
    python setup.py bdist_wheel
    ```

9.  Upload new version of package:
    ```
    twine upload dist/barcode_splitter-<version>-py2.py3-none-any.whl
    ```
    If asked, enter your login for pypi.python.org.

10.  Prepare for new development (can be pushed directly to master):

    1.  Change `__version__` in `barcode_splitter.py` to next version, appending
    `.dev1`.

    2.  Update `CHNAGELOG.md` to add `Unreleased` section, add diff link:
        ```
        https://bitbucket.org/princeton_genomics/barcode_splitter/branches/compare/HEAD%0D<version>
        ```
